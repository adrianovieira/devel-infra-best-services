-- Banco e para o Voucher Digital
CREATE DATABASE voucher_landingpage ENCODING UTF8;
CREATE DATABASE voucher_campanhas ENCODING UTF8;
CREATE DATABASE voucher_manager ENCODING UTF8;

CREATE DATABASE voucher_digital ENCODING UTF8;
\connect voucher_digital;
CREATE SCHEMA IF NOT EXISTS landingpage;
CREATE SCHEMA IF NOT EXISTS campanhas;
CREATE SCHEMA IF NOT EXISTS manager;
