-- base unica de consumidores;
\connect consumidores;

-- consfinal.sq_cons_final_nao_identificado definition

-- DROP SEQUENCE consfinal.sq_cons_final_nao_identificado;

CREATE SEQUENCE consfinal.sq_cons_final_nao_identificado
	MINVALUE 0
	NO MAXVALUE
	START 0
	NO CYCLE;


-- consfinal.sq_consumidor_final definition

-- DROP SEQUENCE consfinal.sq_consumidor_final;

CREATE SEQUENCE consfinal.sq_consumidor_final
	MINVALUE 0
	NO MAXVALUE
	START 0
	NO CYCLE;


-- consfinal.sq_email_contato_cons_final definition

-- DROP SEQUENCE consfinal.sq_email_contato_cons_final;

CREATE SEQUENCE consfinal.sq_email_contato_cons_final
	MINVALUE 0
	NO MAXVALUE
	START 0
	NO CYCLE;


-- consfinal.sq_endereco_contato_cons_final definition

-- DROP SEQUENCE consfinal.sq_endereco_contato_cons_final;

CREATE SEQUENCE consfinal.sq_endereco_contato_cons_final
	MINVALUE 0
	NO MAXVALUE
	START 0
	NO CYCLE;


-- consfinal.sq_origem_contato_cons_final definition

-- DROP SEQUENCE consfinal.sq_origem_contato_cons_final;

CREATE SEQUENCE consfinal.sq_origem_contato_cons_final
	MINVALUE 0
	NO MAXVALUE
	START 0
	NO CYCLE;


-- consfinal.sq_origem_contato_cons_final_nao_id definition

-- DROP SEQUENCE consfinal.sq_origem_contato_cons_final_nao_id;

CREATE SEQUENCE consfinal.sq_origem_contato_cons_final_nao_id
	MINVALUE 0
	NO MAXVALUE
	START 0
	NO CYCLE;


-- consfinal.sq_telefone_contato_cons_final definition

-- DROP SEQUENCE consfinal.sq_telefone_contato_cons_final;

CREATE SEQUENCE consfinal.sq_telefone_contato_cons_final
	MINVALUE 0
	NO MAXVALUE
	START 0
	NO CYCLE;


-- consfinal.sq_tipo_origem_cons_final definition

-- DROP SEQUENCE consfinal.sq_tipo_origem_cons_final;

CREATE SEQUENCE consfinal.sq_tipo_origem_cons_final
	MINVALUE 0
	NO MAXVALUE
	START 0
	NO CYCLE;