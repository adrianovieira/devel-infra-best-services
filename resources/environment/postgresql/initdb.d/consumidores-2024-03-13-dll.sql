-- base unica de consumidores;
\connect consumidores;

-- consfinal.email_contato_cons_final definition

-- Drop table

-- DROP TABLE consfinal.email_contato_cons_final;

CREATE TABLE consfinal.email_contato_cons_final (
	no_seq_email_cons_final numeric NOT NULL, -- Identificador de e-mail de contato do consumidor final.
	ds_email_cons_final varchar(256) NULL, -- E-mail do contato do consumidor final.
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	ds_status_email varchar(25) NULL, -- após a verificação de e-mail, os endereços são classificados de acordo com os critérios da empresa verificadora <safetymails> valores possíveis: válidos; limitados; inválidos; erro de sintaxe; domínio inválido; descartável; junk; spamtrap; scraped; incerto; pendente
	cd_tipo_populadidade_email varchar(50) NULL, -- "informações sobre a incidência de endereços de e-mail em outras listas que passaram pela verificação de e-mail na safetymails. esta informação serve para indicar que determinados endereços de e-mails podem estar sendo muito atingidos por mensagens de e-mail marketing.¶valores possíveis: d1 – raramente difundido; d2 – pouco difundido; d3 – difundido; d4 – muito difundido"
	ds_email_corr varchar(256) NULL, -- sugestão para correção do email enviada pela empresa verificadora <safetymails> .
	dt_log timestamptz NULL, -- campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.
	CONSTRAINT pk_emailctatconsfinal PRIMARY KEY (no_seq_email_cons_final)
);
COMMENT ON TABLE consfinal.email_contato_cons_final IS 'Armazena os dados de e-mail de contato do Consumidor Final';

-- Column comments

COMMENT ON COLUMN consfinal.email_contato_cons_final.no_seq_email_cons_final IS 'Identificador de e-mail de contato do consumidor final.';
COMMENT ON COLUMN consfinal.email_contato_cons_final.ds_email_cons_final IS 'E-mail do contato do consumidor final.';
COMMENT ON COLUMN consfinal.email_contato_cons_final.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.email_contato_cons_final.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.email_contato_cons_final.cd_usuario IS 'Código do usuário responsável pelo registro';
COMMENT ON COLUMN consfinal.email_contato_cons_final.ds_status_email IS 'após a verificação de e-mail, os endereços são classificados de acordo com os critérios da empresa verificadora <safetymails> valores possíveis: válidos; limitados; inválidos; erro de sintaxe; domínio inválido; descartável; junk; spamtrap; scraped; incerto; pendente';
COMMENT ON COLUMN consfinal.email_contato_cons_final.cd_tipo_populadidade_email IS '"informações sobre a incidência de endereços de e-mail em outras listas que passaram pela verificação de e-mail na safetymails. esta informação serve para indicar que determinados endereços de e-mails podem estar sendo muito atingidos por mensagens de e-mail marketing.
valores possíveis: d1 – raramente difundido; d2 – pouco difundido; d3 – difundido; d4 – muito difundido"';
COMMENT ON COLUMN consfinal.email_contato_cons_final.ds_email_corr IS 'sugestão para correção do email enviada pela empresa verificadora <safetymails> .';
COMMENT ON COLUMN consfinal.email_contato_cons_final.dt_log IS 'campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.';


-- consfinal.endereco_contato_cons_final definition

-- Drop table

-- DROP TABLE consfinal.endereco_contato_cons_final;

CREATE TABLE consfinal.endereco_contato_cons_final (
	no_seq_end_cons_final numeric NOT NULL, -- Identificador único de registro de endereço do consumidor final
	nm_logradouro varchar(100) NULL, -- Logradouro do Consumidor Final
	cd_tipo_end bpchar(1) NULL, -- Código que especifica o tipo de endereço (Ex: C - Comerical, R - Residencial)
	ds_tipo_logr varchar(36) NULL, -- Tipo de Logradouro. Ex: Avenida, Rua, Estrada.
	no_logr varchar(20) NULL, -- Número do Logradouro.
	ds_compm_logr varchar(50) NULL, -- Complemento do Logradouro. Ex: Apartamento 401.
	nm_bairro varchar(100) NULL, -- Bairro do Endereço do Contato
	nm_uf varchar(100) NULL, -- Estado da federação
	no_cep varchar(8) NULL, -- CEP do Endereço do Contato
	nm_munic varchar(100) NULL, -- Município do Endereço do Contato
	nm_pais varchar(100) NULL, -- País do Endereço do Contato
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	dt_log timestamptz NULL, -- campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.
	CONSTRAINT endereco_contato_cons_final_cd_tipo_end_check CHECK ((cd_tipo_end = ANY (ARRAY['R'::bpchar, 'C'::bpchar]))),
	CONSTRAINT pk_endctatconsfinal PRIMARY KEY (no_seq_end_cons_final)
);
COMMENT ON TABLE consfinal.endereco_contato_cons_final IS 'Tabela de generalização de endereços relacionados a pessoa consumidora e às suas especializações.';

-- Column comments

COMMENT ON COLUMN consfinal.endereco_contato_cons_final.no_seq_end_cons_final IS 'Identificador único de registro de endereço do consumidor final';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.nm_logradouro IS 'Logradouro do Consumidor Final';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.cd_tipo_end IS 'Código que especifica o tipo de endereço (Ex: C - Comerical, R - Residencial)';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.ds_tipo_logr IS 'Tipo de Logradouro. Ex: Avenida, Rua, Estrada.';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.no_logr IS 'Número do Logradouro.';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.ds_compm_logr IS 'Complemento do Logradouro. Ex: Apartamento 401.';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.nm_bairro IS 'Bairro do Endereço do Contato';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.nm_uf IS 'Estado da federação';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.no_cep IS 'CEP do Endereço do Contato';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.nm_munic IS 'Município do Endereço do Contato';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.nm_pais IS 'País do Endereço do Contato';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.cd_usuario IS 'Código do usuário responsável pelo registro';
COMMENT ON COLUMN consfinal.endereco_contato_cons_final.dt_log IS 'campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.';


-- consfinal.estado_civil_cons_final definition

-- Drop table

-- DROP TABLE consfinal.estado_civil_cons_final;

CREATE TABLE consfinal.estado_civil_cons_final (
	cd_estciv_cons_final numeric NOT NULL, -- Identifica as opções de estado civil do domínio estado civil.
	ds_estciv_cons_final varchar NOT NULL, -- define os estados civis do domínio estado civil.
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	dt_log timestamptz NULL, -- campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.
	CONSTRAINT pk_estcivconsfinal PRIMARY KEY (cd_estciv_cons_final)
);
COMMENT ON TABLE consfinal.estado_civil_cons_final IS 'Tabela domínio do estado Civil do consumidor Final';

-- Column comments

COMMENT ON COLUMN consfinal.estado_civil_cons_final.cd_estciv_cons_final IS 'Identifica as opções de estado civil do domínio estado civil.';
COMMENT ON COLUMN consfinal.estado_civil_cons_final.ds_estciv_cons_final IS 'define os estados civis do domínio estado civil.';
COMMENT ON COLUMN consfinal.estado_civil_cons_final.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.estado_civil_cons_final.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.estado_civil_cons_final.cd_usuario IS 'Código do usuário responsável pelo registro';
COMMENT ON COLUMN consfinal.estado_civil_cons_final.dt_log IS 'campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.';


-- consfinal.genero_cons_final definition

-- Drop table

-- DROP TABLE consfinal.genero_cons_final;

CREATE TABLE consfinal.genero_cons_final (
	cd_genero_cons_final numeric NOT NULL, -- Identifica o gênero do consumidor final
	ds_genero_cons_final varchar(20) NOT NULL, -- Descreve o gênero do consumidor final
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	dt_log timestamptz NULL, -- campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.
	CONSTRAINT pk_genconfinal PRIMARY KEY (cd_genero_cons_final)
);
COMMENT ON TABLE consfinal.genero_cons_final IS 'Tabela dominio de gênero do consumidor final';

-- Column comments

COMMENT ON COLUMN consfinal.genero_cons_final.cd_genero_cons_final IS 'Identifica o gênero do consumidor final';
COMMENT ON COLUMN consfinal.genero_cons_final.ds_genero_cons_final IS 'Descreve o gênero do consumidor final';
COMMENT ON COLUMN consfinal.genero_cons_final.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.genero_cons_final.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.genero_cons_final.cd_usuario IS 'Código do usuário responsável pelo registro';
COMMENT ON COLUMN consfinal.genero_cons_final.dt_log IS 'campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.';


-- consfinal.telefone_contato_cons_final definition

-- Drop table

-- DROP TABLE consfinal.telefone_contato_cons_final;

CREATE TABLE consfinal.telefone_contato_cons_final (
	no_seq_tel_cons_final numeric NOT NULL, -- Identificador sequencial do telefone de contato associado ao consumidor Final.
	no_ddd numeric(3) NULL, -- DDD do telefone de contato do consumidor final
	no_tel_ctat numeric(12) NULL, -- Número do telefone de contato do consumidor final
	cd_tipo_ctat numeric(1) NULL, -- Identifica o tipo de telefone para contato (Ex: 1 - Celular, 2 - Residencial, 3 - Comercial)
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	cd_status_tel_enriquecido bpchar(1) NULL,
	no_ddd_enriquecido numeric(3) NULL,
	no_tel_ctat_enriquecido numeric(12) NULL,
	dt_log timestamptz NULL, -- campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.
	CONSTRAINT pk_telctatconsfinal PRIMARY KEY (no_seq_tel_cons_final)
);

-- Column comments

COMMENT ON COLUMN consfinal.telefone_contato_cons_final.no_seq_tel_cons_final IS 'Identificador sequencial do telefone de contato associado ao consumidor Final.';
COMMENT ON COLUMN consfinal.telefone_contato_cons_final.no_ddd IS 'DDD do telefone de contato do consumidor final';
COMMENT ON COLUMN consfinal.telefone_contato_cons_final.no_tel_ctat IS 'Número do telefone de contato do consumidor final';
COMMENT ON COLUMN consfinal.telefone_contato_cons_final.cd_tipo_ctat IS 'Identifica o tipo de telefone para contato (Ex: 1 - Celular, 2 - Residencial, 3 - Comercial)';
COMMENT ON COLUMN consfinal.telefone_contato_cons_final.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.telefone_contato_cons_final.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.telefone_contato_cons_final.cd_usuario IS 'Código do usuário responsável pelo registro';
COMMENT ON COLUMN consfinal.telefone_contato_cons_final.dt_log IS 'campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.';


-- consfinal.tipo_origem_cons_final definition

-- Drop table

-- DROP TABLE consfinal.tipo_origem_cons_final;

CREATE TABLE consfinal.tipo_origem_cons_final (
	no_seq_tipo_orig_cons_final numeric NOT NULL, -- Número sequencial identificador do tipo de origem do consumidor final
	ds_tipo_orig_cons_final varchar(100) NOT NULL, -- Descrição do tipo de origem do consumidor KM de Vantagens, JETOIL, VOUCHER, etc.
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	dt_log timestamptz NULL, -- campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.
	CONSTRAINT pk_tipoorigconsfinal PRIMARY KEY (no_seq_tipo_orig_cons_final)
);
COMMENT ON TABLE consfinal.tipo_origem_cons_final IS 'Tabela de  dominio que Identifica o tipo de origem do consumidor: KM de Vantagens, JETOIL, VOUCHER, etc.';

-- Column comments

COMMENT ON COLUMN consfinal.tipo_origem_cons_final.no_seq_tipo_orig_cons_final IS 'Número sequencial identificador do tipo de origem do consumidor final';
COMMENT ON COLUMN consfinal.tipo_origem_cons_final.ds_tipo_orig_cons_final IS 'Descrição do tipo de origem do consumidor KM de Vantagens, JETOIL, VOUCHER, etc.';
COMMENT ON COLUMN consfinal.tipo_origem_cons_final.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.tipo_origem_cons_final.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.tipo_origem_cons_final.cd_usuario IS 'Código do usuário responsável pelo registro';
COMMENT ON COLUMN consfinal.tipo_origem_cons_final.dt_log IS 'campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.';


-- consfinal.consumidor_final_nao_ident definition

-- Drop table

-- DROP TABLE consfinal.consumidor_final_nao_ident;

CREATE TABLE consfinal.consumidor_final_nao_ident (
	no_seq_cons_final_nao_ident numeric NOT NULL, -- Identificação do consumidor NÃO IDENTIFICADO no ambiente Ipiranga
	nm_compl_cons_final_nao_ident varchar(256) NULL, -- NOME COMPLETO DO CONSUMIDOR FINAL NÃO IDENTIFICADO
	nm_social_cons_final_nao_ident varchar(60) NULL, -- NOME SOCIAL DO CONSUMIDOR fINAL NÃO IDENTIFICADO
	dt_nasc_cons_final_nao_ident date NULL, -- DATA DE NASCIMENTO DO CONSUMIDOR fINAL NÃO IDENTIFICADO
	dt_incl timestamptz NOT NULL, -- DATA DE INCLUSÃO DO REGISTRO
	cd_usuario varchar(20) NOT NULL, -- CÓDIGO DE USUÁRIO DE REGISTRO
	cd_estciv_cons_final_nao_ident numeric NULL, -- Identifica as opções de estado civil do domínio estado civil.
	cd_genero_cons_final_nao_ident numeric NULL, -- Identifica o gênero do consumidor final
	dt_alter timestamptz NULL, -- Data de Alteração
	dt_log timestamptz NULL, -- campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.
	CONSTRAINT pk_cofinaid PRIMARY KEY (no_seq_cons_final_nao_ident),
	CONSTRAINT fk_cofinaid_estconfi_01 FOREIGN KEY (cd_estciv_cons_final_nao_ident) REFERENCES consfinal.estado_civil_cons_final(cd_estciv_cons_final),
	CONSTRAINT fk_cofinaid_genconfi_01 FOREIGN KEY (cd_genero_cons_final_nao_ident) REFERENCES consfinal.genero_cons_final(cd_genero_cons_final)
);
CREATE INDEX if_cofinaid_01 ON consfinal.consumidor_final_nao_ident USING btree (cd_genero_cons_final_nao_ident);
CREATE INDEX if_cofinaid_02 ON consfinal.consumidor_final_nao_ident USING btree (cd_estciv_cons_final_nao_ident);
COMMENT ON TABLE consfinal.consumidor_final_nao_ident IS 'Tabela de generalização de pessoa consumidora dos produtos e serviços Ipiranga. 
 Nesta tabela estão os consumidores que não puderão ser identificados atraves do CIAM ( consumidores SEM CPF)';

-- Column comments

COMMENT ON COLUMN consfinal.consumidor_final_nao_ident.no_seq_cons_final_nao_ident IS 'Identificação do consumidor NÃO IDENTIFICADO no ambiente Ipiranga';
COMMENT ON COLUMN consfinal.consumidor_final_nao_ident.nm_compl_cons_final_nao_ident IS 'NOME COMPLETO DO CONSUMIDOR FINAL NÃO IDENTIFICADO';
COMMENT ON COLUMN consfinal.consumidor_final_nao_ident.nm_social_cons_final_nao_ident IS 'NOME SOCIAL DO CONSUMIDOR fINAL NÃO IDENTIFICADO';
COMMENT ON COLUMN consfinal.consumidor_final_nao_ident.dt_nasc_cons_final_nao_ident IS 'DATA DE NASCIMENTO DO CONSUMIDOR fINAL NÃO IDENTIFICADO';
COMMENT ON COLUMN consfinal.consumidor_final_nao_ident.dt_incl IS 'DATA DE INCLUSÃO DO REGISTRO';
COMMENT ON COLUMN consfinal.consumidor_final_nao_ident.cd_usuario IS 'CÓDIGO DE USUÁRIO DE REGISTRO';
COMMENT ON COLUMN consfinal.consumidor_final_nao_ident.cd_estciv_cons_final_nao_ident IS 'Identifica as opções de estado civil do domínio estado civil.';
COMMENT ON COLUMN consfinal.consumidor_final_nao_ident.cd_genero_cons_final_nao_ident IS 'Identifica o gênero do consumidor final';
COMMENT ON COLUMN consfinal.consumidor_final_nao_ident.dt_alter IS 'Data de Alteração';
COMMENT ON COLUMN consfinal.consumidor_final_nao_ident.dt_log IS 'campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.';


-- consfinal.origem_cons_final_nao_ident definition

-- Drop table

-- DROP TABLE consfinal.origem_cons_final_nao_ident;

CREATE TABLE consfinal.origem_cons_final_nao_ident (
	nu_seq_final_nao_ident numeric(10) NULL, -- Identificação do consumidor NÃO IDENTIFICADO no ambiente Ipiranga
	no_seq_tipo_orig_cons_final numeric NULL, -- Número sequencial identificador do tipo de origem do consumidor final
	CONSTRAINT fk_orcofini_cofinaid_01 FOREIGN KEY (nu_seq_final_nao_ident) REFERENCES consfinal.consumidor_final_nao_ident(no_seq_cons_final_nao_ident),
	CONSTRAINT fk_orcofini_tipoorig_01 FOREIGN KEY (no_seq_tipo_orig_cons_final) REFERENCES consfinal.tipo_origem_cons_final(no_seq_tipo_orig_cons_final)
);
CREATE INDEX if_orcofini_01 ON consfinal.origem_cons_final_nao_ident USING btree (no_seq_tipo_orig_cons_final);
CREATE INDEX if_orcofini_02 ON consfinal.origem_cons_final_nao_ident USING btree (nu_seq_final_nao_ident);
COMMENT ON TABLE consfinal.origem_cons_final_nao_ident IS 'ARMAZENA AS INFORMAÇÃOS DO TIPO DE ORIGEMS DO CONSUMIDOR FINAL NÃO IDENTIFICADO';

-- Column comments

COMMENT ON COLUMN consfinal.origem_cons_final_nao_ident.nu_seq_final_nao_ident IS 'Identificação do consumidor NÃO IDENTIFICADO no ambiente Ipiranga';
COMMENT ON COLUMN consfinal.origem_cons_final_nao_ident.no_seq_tipo_orig_cons_final IS 'Número sequencial identificador do tipo de origem do consumidor final';


-- consfinal.origem_ctat_cons_final_nao_id definition

-- Drop table

-- DROP TABLE consfinal.origem_ctat_cons_final_nao_id;

CREATE TABLE consfinal.origem_ctat_cons_final_nao_id (
	no_seq_ctat_cons_final_nao_id numeric NOT NULL, --  Identificação única do consumidor não identificado (número sequencial).
	no_seq_cons_final_nao_ident numeric NOT NULL,
	no_seq_tipo_orig_cons_final numeric NOT NULL, -- Número sequencial identificador do tipo de origem do consumidor final
	no_seq_end_cons_final numeric NULL, -- Identificador único de registro de endereço do consumidor final
	no_seq_tel_cons_final numeric NULL, -- Identificador sequencial do telefone de contato associado ao consumidor Final.
	no_seq_email_cons_final numeric NULL, -- Identificador de e-mail de contato do consumidor final.
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	dt_inat timestamptz NULL,
	dt_reat timestamptz NULL,
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	dt_log timestamptz NULL, -- campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.
	CONSTRAINT pk_origctatconsfinalnaoid PRIMARY KEY (no_seq_ctat_cons_final_nao_id),
	CONSTRAINT fk_orctccofinid_consfinalni_01 FOREIGN KEY (no_seq_cons_final_nao_ident) REFERENCES consfinal.consumidor_final_nao_ident(no_seq_cons_final_nao_ident),
	CONSTRAINT fk_orctcofinid_emaictat_01 FOREIGN KEY (no_seq_email_cons_final) REFERENCES consfinal.email_contato_cons_final(no_seq_email_cons_final),
	CONSTRAINT fk_orctcofinid_endctat_01 FOREIGN KEY (no_seq_end_cons_final) REFERENCES consfinal.endereco_contato_cons_final(no_seq_end_cons_final),
	CONSTRAINT fk_orctcofinid_telctat_01 FOREIGN KEY (no_seq_tel_cons_final) REFERENCES consfinal.telefone_contato_cons_final(no_seq_tel_cons_final),
	CONSTRAINT fk_orctcofinid_tipoorig_01 FOREIGN KEY (no_seq_tipo_orig_cons_final) REFERENCES consfinal.tipo_origem_cons_final(no_seq_tipo_orig_cons_final)
);

-- Column comments

COMMENT ON COLUMN consfinal.origem_ctat_cons_final_nao_id.no_seq_ctat_cons_final_nao_id IS ' Identificação única do consumidor não identificado (número sequencial).';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final_nao_id.no_seq_tipo_orig_cons_final IS 'Número sequencial identificador do tipo de origem do consumidor final';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final_nao_id.no_seq_end_cons_final IS 'Identificador único de registro de endereço do consumidor final';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final_nao_id.no_seq_tel_cons_final IS 'Identificador sequencial do telefone de contato associado ao consumidor Final.';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final_nao_id.no_seq_email_cons_final IS 'Identificador de e-mail de contato do consumidor final.';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final_nao_id.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final_nao_id.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final_nao_id.cd_usuario IS 'Código do usuário responsável pelo registro';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final_nao_id.dt_log IS 'campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.';


-- consfinal.consumidor_final definition

-- Drop table

-- DROP TABLE consfinal.consumidor_final;

CREATE TABLE consfinal.consumidor_final (
	no_seq_cons_final numeric NOT NULL, -- Identificação única do consumidor no ambiente Ipiranga (número sequencial).
	cd_pess_autent varchar(40) NOT NULL, -- Identificador único de pessoa recebido pelo autenticador (CIAM).
	no_cpf_cons_final varchar(11) NOT NULL, -- CPF do consumidor final com dígito verificador.¶Oriundo do autenticador
	no_tel_cons_final numeric(14) NOT NULL, -- Número de telefone (DDD+ NÚMERO) para identificação do consumidor final no sistema (celular). Oriundo do autenticador¶serve como telefone de Cadastro do consumidor final
	ds_email_cons_final varchar(256) NOT NULL, -- E-mail do consumidor final para identificação no sistema. Trata-se de E-mail de cadastro oriundo do autenticador
	nm_compl_cons_final varchar(256) NULL, -- Nome completo do consumidor final
	nm_social_cons_final varchar(60) NULL, -- Nome de identificação do consumidor final
	dt_nasc_cons_final date NULL, -- Data de nascimento do consumidor final.
	cd_genero_cons_final numeric NULL, -- Identifica o gênero do consumidor final
	cd_estciv_cons_final numeric NULL, -- Identifica as opções de estado civil do domínio estado civil.
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	dt_inat timestamptz NULL, -- Data de inativação do registro
	dt_reat timestamptz NULL, -- Data de Reativação do registro
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pela carga do registro
	no_seq_cons_final_nao_ident numeric NULL, -- Chave identificador que liga ao consumidoir final não identificado ( que não possuia CPF cadastrado)
	dt_log timestamptz NULL,
	CONSTRAINT pk_consfinal PRIMARY KEY (no_seq_cons_final),
	CONSTRAINT fk_consfina_estconfi_01 FOREIGN KEY (cd_estciv_cons_final) REFERENCES consfinal.estado_civil_cons_final(cd_estciv_cons_final),
	CONSTRAINT fk_consfina_genconfi_01 FOREIGN KEY (cd_genero_cons_final) REFERENCES consfinal.genero_cons_final(cd_genero_cons_final),
	CONSTRAINT fk_consfinalnident_01 FOREIGN KEY (no_seq_cons_final_nao_ident) REFERENCES consfinal.consumidor_final_nao_ident(no_seq_cons_final_nao_ident)
);
CREATE INDEX if_consfinal_01 ON consfinal.consumidor_final USING btree (cd_genero_cons_final);
CREATE INDEX if_consfinal_02 ON consfinal.consumidor_final USING btree (cd_estciv_cons_final);
CREATE UNIQUE INDEX iu_consfinal_01 ON consfinal.consumidor_final USING btree (no_cpf_cons_final);
CREATE UNIQUE INDEX iu_consfinal_02 ON consfinal.consumidor_final USING btree (no_tel_cons_final);
CREATE UNIQUE INDEX iu_consfinal_03 ON consfinal.consumidor_final USING btree (ds_email_cons_final);
COMMENT ON TABLE consfinal.consumidor_final IS 'Tabela de generalização de pessoa consumidora dos produtos e serviços Ipiranga. ';

-- Column comments

COMMENT ON COLUMN consfinal.consumidor_final.no_seq_cons_final IS 'Identificação única do consumidor no ambiente Ipiranga (número sequencial).';
COMMENT ON COLUMN consfinal.consumidor_final.cd_pess_autent IS 'Identificador único de pessoa recebido pelo autenticador (CIAM).';
COMMENT ON COLUMN consfinal.consumidor_final.no_cpf_cons_final IS 'CPF do consumidor final com dígito verificador.
Oriundo do autenticador';
COMMENT ON COLUMN consfinal.consumidor_final.no_tel_cons_final IS 'Número de telefone (DDD+ NÚMERO) para identificação do consumidor final no sistema (celular). Oriundo do autenticador
serve como telefone de Cadastro do consumidor final';
COMMENT ON COLUMN consfinal.consumidor_final.ds_email_cons_final IS 'E-mail do consumidor final para identificação no sistema. Trata-se de E-mail de cadastro oriundo do autenticador';
COMMENT ON COLUMN consfinal.consumidor_final.nm_compl_cons_final IS 'Nome completo do consumidor final';
COMMENT ON COLUMN consfinal.consumidor_final.nm_social_cons_final IS 'Nome de identificação do consumidor final';
COMMENT ON COLUMN consfinal.consumidor_final.dt_nasc_cons_final IS 'Data de nascimento do consumidor final.';
COMMENT ON COLUMN consfinal.consumidor_final.cd_genero_cons_final IS 'Identifica o gênero do consumidor final';
COMMENT ON COLUMN consfinal.consumidor_final.cd_estciv_cons_final IS 'Identifica as opções de estado civil do domínio estado civil.';
COMMENT ON COLUMN consfinal.consumidor_final.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.consumidor_final.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.consumidor_final.dt_inat IS 'Data de inativação do registro';
COMMENT ON COLUMN consfinal.consumidor_final.dt_reat IS 'Data de Reativação do registro';
COMMENT ON COLUMN consfinal.consumidor_final.cd_usuario IS 'Código do usuário responsável pela carga do registro';
COMMENT ON COLUMN consfinal.consumidor_final.no_seq_cons_final_nao_ident IS 'Chave identificador que liga ao consumidoir final não identificado ( que não possuia CPF cadastrado)';


-- consfinal.consumidor_final_jetoil definition

-- Drop table

-- DROP TABLE consfinal.consumidor_final_jetoil;

CREATE TABLE consfinal.consumidor_final_jetoil (
	cd_cad_cons_final_jetoil numeric NOT NULL, -- Número identificador de consumidor final oriundo da tabela do JETOIL localizada em ORABIF
	no_seq_cons_final numeric NULL, -- Identificação única do consumidor no ambiente Ipiranga (número sequencial).
	cd_tipo_cons_final bpchar(2) NULL, -- Identifica se o consumidor final e pessoa fisíca (PF) ou pessoa juridica (PJ)
	dt_nasc_cons_final date NULL, -- Data de nascimento do consumidor participante do JETOIL. Composto pela concatenação dos campos da tabela de origem do JETOIL que contém separadamente Ano, mês e dia. A data de nascimento está representada também nesta entidade para atender necessidade de manter a origem para o Gestão de Campanhas.
	nm_cons_final varchar(50) NULL, -- Identifica como o consumidor prefere ser chamado.
	cd_genero_cons_final numeric NULL, -- Identifica o gênero do consumidor final
	cd_estciv_cons_final numeric NULL, -- Identifica as opções de estado civil do domínio estado civil.
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	dt_inat timestamptz NULL, -- Data de Inativação do registro
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	no_seq_cons_final_nao_ident numeric NULL, -- Identificação do consumidor final que não pode receber um id do autenticador
	dt_log timestamptz NULL,
	CONSTRAINT consumidor_final_jetoil_cd_tipo_cons_final_check CHECK ((cd_tipo_cons_final = ANY (ARRAY['PF'::bpchar, 'PJ'::bpchar]))),
	CONSTRAINT pk_confinaljetoil PRIMARY KEY (cd_cad_cons_final_jetoil),
	CONSTRAINT fk_confinje_consfina_01 FOREIGN KEY (no_seq_cons_final) REFERENCES consfinal.consumidor_final(no_seq_cons_final),
	CONSTRAINT fk_confinje_estconfi_01 FOREIGN KEY (cd_estciv_cons_final) REFERENCES consfinal.estado_civil_cons_final(cd_estciv_cons_final),
	CONSTRAINT fk_confinje_genconfi_01 FOREIGN KEY (cd_genero_cons_final) REFERENCES consfinal.genero_cons_final(cd_genero_cons_final)
);
CREATE INDEX if_confinaljetoil_01 ON consfinal.consumidor_final_jetoil USING btree (cd_genero_cons_final);
CREATE INDEX if_confinaljetoil_02 ON consfinal.consumidor_final_jetoil USING btree (cd_estciv_cons_final);
CREATE INDEX if_confinaljetoil_03 ON consfinal.consumidor_final_jetoil USING btree (no_seq_cons_final);
COMMENT ON TABLE consfinal.consumidor_final_jetoil IS 'Tabela de especialização do modelo de Consumidor Final Ipiranga. Visa manter os dados específicos relacionados ao produto (BU) JETOIL. Seus dados são oriúndos da tabela ORABIF.DBFRANQ.CONSUMIDOR_FINAL_JETOIL.';

-- Column comments

COMMENT ON COLUMN consfinal.consumidor_final_jetoil.cd_cad_cons_final_jetoil IS 'Número identificador de consumidor final oriundo da tabela do JETOIL localizada em ORABIF';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.no_seq_cons_final IS 'Identificação única do consumidor no ambiente Ipiranga (número sequencial).';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.cd_tipo_cons_final IS 'Identifica se o consumidor final e pessoa fisíca (PF) ou pessoa juridica (PJ)';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.dt_nasc_cons_final IS 'Data de nascimento do consumidor participante do JETOIL. Composto pela concatenação dos campos da tabela de origem do JETOIL que contém separadamente Ano, mês e dia. A data de nascimento está representada também nesta entidade para atender necessidade de manter a origem para o Gestão de Campanhas.';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.nm_cons_final IS 'Identifica como o consumidor prefere ser chamado.';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.cd_genero_cons_final IS 'Identifica o gênero do consumidor final';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.cd_estciv_cons_final IS 'Identifica as opções de estado civil do domínio estado civil.';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.dt_inat IS 'Data de Inativação do registro';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.cd_usuario IS 'Código do usuário responsável pelo registro';
COMMENT ON COLUMN consfinal.consumidor_final_jetoil.no_seq_cons_final_nao_ident IS 'Identificação do consumidor final que não pode receber um id do autenticador';


-- consfinal.consumidor_final_kmvant definition

-- Drop table

-- DROP TABLE consfinal.consumidor_final_kmvant;

CREATE TABLE consfinal.consumidor_final_kmvant (
	no_seq_cons_final_kmvant numeric NOT NULL, -- Numero sequencial do consumidor final para o Km de Vantagens no DW LEGADO.
	no_seq_cons_final numeric NULL, -- Identificação única do consumidor no ambiente Ipiranga (número sequencial).
	nm_apel_partic varchar(60) NULL, -- Nome do apelido do participante
	cd_estciv_cons_final numeric NULL, -- Identifica as opções de estado civil do domínio estado civil.
	cd_tipo_orig_categ numeric(1) NULL, -- Código de categorização em função do TIPO DE ORIGEM. Ex:Exemplo:¶0    - Não Aplicável¶1    - Automática¶2    - Manual¶3    - Mudança Perfil¶4    - Cartão Ipiranga
	ds_tipo_partic bpchar(1) NULL, -- Identifica se o participante é autônomo ou não (S para Sim/N para Não)
	cd_tipo_categ_cons_kmvant numeric(8) NULL, -- Código que identifica a categoria do consumidor final. -1 - Não Aplicável¶1 - Básica¶2 - Prestige¶3 - Unique¶4 - Caminhoneiro
	cd_orig_site_kmvant numeric(3) NULL, -- Código sequencial que identifica a origem do cadastro do consumidor que a Mimética nos envia. Os consumidores podem se cadastrar pelo site, pela central de atendimento (Sercon), etc. Ex: POS Ipiranga, Website KM de Vantagens, Aplicativo Abastece aí, etc. 1    Franquia Ipiranga¶2    Tlmk Ipiranga¶3    Ipirangashop Ipirang¶4    POS Ipiranga¶5    Site¶6    1 Milhão De Litros I¶7    Franquia Texaco¶8    Tlmk Texaco¶9    Ipirangashop Texaco¶10    POS Texaco. Demais descrições podem ser encontradas na tabela DM_ORIGEM_SITE_KMVANT situada em ORABIF.
	id_uso_autorz_dado_pess_kmvant numeric(1) NULL, -- Indica se o consumidor final do KM de Vantagens autorizou a utilização de seus dados pessoais para ofertas de novos produtos e servicos.
	dt_prim_ctat_partic_kmvant date NULL, -- Data do primeiro contato do participante com o programa, seja ele o cadastramento no site ou alguma transação realizada.
	id_cad_compl bpchar(1) NULL, -- Identifica se o cadastro está completo ou não
	no_seq_cons_final_mimet numeric(10) NULL, -- Número sequencial do consumidor no EAI (Mimética). Optamos por armazenar esta informação para facilitar investigações que possam surgir futuramente.
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	dt_inat timestamptz NULL, -- Data de inativação do registro
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	nm_consumifor_final_kmvant varchar(60) NULL,
	dt_log timestamptz NULL,
	CONSTRAINT consumidor_final_kmvant_ds_tipo_partic_check CHECK ((ds_tipo_partic = ANY (ARRAY['S'::bpchar, 'N'::bpchar]))),
	CONSTRAINT pk_confinalkmvant PRIMARY KEY (no_seq_cons_final_kmvant),
	CONSTRAINT fk_cofikmva_consfina_01 FOREIGN KEY (no_seq_cons_final) REFERENCES consfinal.consumidor_final(no_seq_cons_final),
	CONSTRAINT fk_cofikmva_estconfi_01 FOREIGN KEY (cd_estciv_cons_final) REFERENCES consfinal.estado_civil_cons_final(cd_estciv_cons_final)
);
CREATE INDEX if_confinalkmvant_01 ON consfinal.consumidor_final_kmvant USING btree (no_seq_cons_final);
CREATE INDEX if_confinalkmvant_02 ON consfinal.consumidor_final_kmvant USING btree (cd_estciv_cons_final);
COMMENT ON TABLE consfinal.consumidor_final_kmvant IS 'Tabela de especialização do modelo de Consumidor Final Ipiranga. Visa manter os dados específicos relacionados ao produto (BU)  KMVANT Seus dados são oriúndos da tabela ORABIF.DBFRANQ.CONSUMIDOR_FINAL_KMVANT.';

-- Column comments

COMMENT ON COLUMN consfinal.consumidor_final_kmvant.no_seq_cons_final_kmvant IS 'Numero sequencial do consumidor final para o Km de Vantagens no DW LEGADO.';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.no_seq_cons_final IS 'Identificação única do consumidor no ambiente Ipiranga (número sequencial).';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.nm_apel_partic IS 'Nome do apelido do participante';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.cd_estciv_cons_final IS 'Identifica as opções de estado civil do domínio estado civil.';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.cd_tipo_orig_categ IS 'Código de categorização em função do TIPO DE ORIGEM. Ex:Exemplo:
0    - Não Aplicável
1    - Automática
2    - Manual
3    - Mudança Perfil
4    - Cartão Ipiranga';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.ds_tipo_partic IS 'Identifica se o participante é autônomo ou não (S para Sim/N para Não)';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.cd_tipo_categ_cons_kmvant IS 'Código que identifica a categoria do consumidor final. -1 - Não Aplicável
1 - Básica
2 - Prestige
3 - Unique
4 - Caminhoneiro';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.cd_orig_site_kmvant IS 'Código sequencial que identifica a origem do cadastro do consumidor que a Mimética nos envia. Os consumidores podem se cadastrar pelo site, pela central de atendimento (Sercon), etc. Ex: POS Ipiranga, Website KM de Vantagens, Aplicativo Abastece aí, etc. 1    Franquia Ipiranga
2    Tlmk Ipiranga
3    Ipirangashop Ipirang
4    POS Ipiranga
5    Site
6    1 Milhão De Litros I
7    Franquia Texaco
8    Tlmk Texaco
9    Ipirangashop Texaco
10    POS Texaco. Demais descrições podem ser encontradas na tabela DM_ORIGEM_SITE_KMVANT situada em ORABIF.';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.id_uso_autorz_dado_pess_kmvant IS 'Indica se o consumidor final do KM de Vantagens autorizou a utilização de seus dados pessoais para ofertas de novos produtos e servicos.';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.dt_prim_ctat_partic_kmvant IS 'Data do primeiro contato do participante com o programa, seja ele o cadastramento no site ou alguma transação realizada.';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.id_cad_compl IS 'Identifica se o cadastro está completo ou não';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.no_seq_cons_final_mimet IS 'Número sequencial do consumidor no EAI (Mimética). Optamos por armazenar esta informação para facilitar investigações que possam surgir futuramente.';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.dt_inat IS 'Data de inativação do registro';
COMMENT ON COLUMN consfinal.consumidor_final_kmvant.cd_usuario IS 'Código do usuário responsável pelo registro';


-- consfinal.origem_cons_final definition

-- Drop table

-- DROP TABLE consfinal.origem_cons_final;

CREATE TABLE consfinal.origem_cons_final (
	no_seq_tipo_orig_cons_final numeric NOT NULL, -- Número sequencial identificador do tipo de origem do consumidor final
	no_seq_cons_final numeric NOT NULL, -- Identificação única do consumidor no ambiente Ipiranga (número sequencial).
	dt_incl timestamp NOT NULL, -- Data de inclusão do registro
	dt_alter timestamp NULL, -- Data de alteração do registro
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	dt_log timestamptz NULL, -- campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.
	CONSTRAINT pk_oriconfinal PRIMARY KEY (no_seq_tipo_orig_cons_final, no_seq_cons_final),
	CONSTRAINT fk_oriconfi_tipoorig_01 FOREIGN KEY (no_seq_tipo_orig_cons_final) REFERENCES consfinal.tipo_origem_cons_final(no_seq_tipo_orig_cons_final),
	CONSTRAINT refconsumidor_final42 FOREIGN KEY (no_seq_cons_final) REFERENCES consfinal.consumidor_final(no_seq_cons_final)
);
CREATE INDEX if_oriconfinal_02 ON consfinal.origem_cons_final USING btree (no_seq_tipo_orig_cons_final);
COMMENT ON TABLE consfinal.origem_cons_final IS 'ARMAZENA AS INFORMAÇÃOS DO TIPO DE ORIGEMS DO CONSUMIDOR FINAL  IDENTIFICADO';

-- Column comments

COMMENT ON COLUMN consfinal.origem_cons_final.no_seq_tipo_orig_cons_final IS 'Número sequencial identificador do tipo de origem do consumidor final';
COMMENT ON COLUMN consfinal.origem_cons_final.no_seq_cons_final IS 'Identificação única do consumidor no ambiente Ipiranga (número sequencial).';
COMMENT ON COLUMN consfinal.origem_cons_final.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.origem_cons_final.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.origem_cons_final.cd_usuario IS 'Código do usuário responsável pelo registro';
COMMENT ON COLUMN consfinal.origem_cons_final.dt_log IS 'campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.';


-- consfinal.origem_ctat_cons_final definition

-- Drop table

-- DROP TABLE consfinal.origem_ctat_cons_final;

CREATE TABLE consfinal.origem_ctat_cons_final (
	no_seq_ctat_cons_final numeric NOT NULL, -- Chave identificadora de Contato do Consumidor Final
	no_seq_cons_final numeric NOT NULL, -- Identificação única do consumidor no ambiente Ipiranga (número sequencial).
	no_seq_tipo_orig_cons_final numeric NOT NULL, -- Número sequencial identificador do tipo de origem do consumidor final
	no_seq_end_cons_final numeric NULL, -- Identificador único de registro de endereço do consumidor final
	no_seq_tel_cons_final numeric NULL, -- Identificador sequencial do telefone de contato associado ao consumidor Final.
	no_seq_email_cons_final numeric NULL, -- Identificador de e-mail de contato do consumidor final.
	dt_incl timestamptz NOT NULL, -- Data de inclusão do registro
	dt_alter timestamptz NULL, -- Data de alteração do registro
	dt_inat timestamptz NULL,
	dt_reat timestamptz NULL,
	cd_usuario varchar(20) NOT NULL, -- Código do usuário responsável pelo registro
	dt_log timestamptz NULL, -- campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.
	CONSTRAINT pk_origctatconsfinal PRIMARY KEY (no_seq_ctat_cons_final),
	CONSTRAINT fk_orctcofi_consfina_01 FOREIGN KEY (no_seq_cons_final) REFERENCES consfinal.consumidor_final(no_seq_cons_final),
	CONSTRAINT fk_orctcofi_emaictat_01 FOREIGN KEY (no_seq_email_cons_final) REFERENCES consfinal.email_contato_cons_final(no_seq_email_cons_final),
	CONSTRAINT fk_orctcofi_endctat_01 FOREIGN KEY (no_seq_end_cons_final) REFERENCES consfinal.endereco_contato_cons_final(no_seq_end_cons_final),
	CONSTRAINT fk_orctcofi_telctat_01 FOREIGN KEY (no_seq_tel_cons_final) REFERENCES consfinal.telefone_contato_cons_final(no_seq_tel_cons_final),
	CONSTRAINT fk_orctcofi_tipoorig_01 FOREIGN KEY (no_seq_tipo_orig_cons_final) REFERENCES consfinal.tipo_origem_cons_final(no_seq_tipo_orig_cons_final)
);
CREATE INDEX if_origctatconsfinal_01 ON consfinal.origem_ctat_cons_final USING btree (no_seq_cons_final);
CREATE INDEX if_origctatconsfinal_02 ON consfinal.origem_ctat_cons_final USING btree (no_seq_tipo_orig_cons_final);
CREATE INDEX if_origctatconsfinal_03 ON consfinal.origem_ctat_cons_final USING btree (no_seq_end_cons_final);
CREATE INDEX if_origctatconsfinal_05 ON consfinal.origem_ctat_cons_final USING btree (no_seq_email_cons_final);
CREATE INDEX if_origctatconsfinali_04 ON consfinal.origem_ctat_cons_final USING btree (no_seq_tel_cons_final);
CREATE INDEX if_origctatconsfinalinaoid_04 ON consfinal.origem_ctat_cons_final USING btree (no_seq_tel_cons_final);
CREATE INDEX if_origctatconsfinalnaoid_02 ON consfinal.origem_ctat_cons_final USING btree (no_seq_tipo_orig_cons_final);
CREATE INDEX if_origctatconsfinalnaoid_03 ON consfinal.origem_ctat_cons_final USING btree (no_seq_end_cons_final);
CREATE INDEX if_origctatconsfinalnaoid_05 ON consfinal.origem_ctat_cons_final USING btree (no_seq_email_cons_final);
COMMENT ON TABLE consfinal.origem_ctat_cons_final IS 'Identifica as origens de contato do consumidor final';

-- Column comments

COMMENT ON COLUMN consfinal.origem_ctat_cons_final.no_seq_ctat_cons_final IS 'Chave identificadora de Contato do Consumidor Final';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final.no_seq_cons_final IS 'Identificação única do consumidor no ambiente Ipiranga (número sequencial).';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final.no_seq_tipo_orig_cons_final IS 'Número sequencial identificador do tipo de origem do consumidor final';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final.no_seq_end_cons_final IS 'Identificador único de registro de endereço do consumidor final';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final.no_seq_tel_cons_final IS 'Identificador sequencial do telefone de contato associado ao consumidor Final.';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final.no_seq_email_cons_final IS 'Identificador de e-mail de contato do consumidor final.';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final.dt_incl IS 'Data de inclusão do registro';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final.dt_alter IS 'Data de alteração do registro';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final.cd_usuario IS 'Código do usuário responsável pelo registro';
COMMENT ON COLUMN consfinal.origem_ctat_cons_final.dt_log IS 'campo que monitora as alterações e inclusões de registros na entidade. Usado para propósito de integração incremental de dados.';