-- base unica de consumidores;
\connect consumidores;

INSERT INTO consfinal.consumidor_final 
(
  no_seq_cons_final,
  cd_pess_autent,
  nm_compl_cons_final,
  dt_nasc_cons_final,
  no_cpf_cons_final,
  no_tel_cons_final,
  ds_email_cons_final,
  dt_incl,
  cd_usuario
) 
VALUES (
  1,
  '4dc4f112-41e1-4a49-805e-fb6abfb254f1',
  'Bob Fellow',
  '2000-02-29',
  '619876541',
  '12345678901',
  'bob.48053162924@mail.me',
  now(),
  'Bob'
),(
  2,
  'e27c7254-178c-4bfb-b814-137a20cf99f3',
  'John Snow',
  '1910-10-10',
  '12312312345',
  '219876542',
  'johnsnow@gmail.com',
  now(),
  'Snow'
);
