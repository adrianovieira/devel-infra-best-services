-- Banco e usuário para o SonarQube
CREATE USER sonarqube WITH PASSWORD 'notsecure';
CREATE DATABASE sonarqube ENCODING UTF8 OWNER sonarqube;
