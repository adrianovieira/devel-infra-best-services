#!/bin/sh

aws s3 cp \
       s3://qa-reports.voucher.radixengsoft/ \
       s3://qa-reports.voucher.radixengsoft/ \
       --exclude '*' \
       --include '*.html' \
       --no-guess-mime-type \
       --content-type="text/html" \
       --metadata-directive="REPLACE" \
       --recursive
